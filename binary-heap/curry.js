const curry = func => {
  const _internal = (memo = []) => (...args) => {
    const _memo = [...memo, ...args]

    if (_memo.length < func.length) {
      return _internal(_memo)
    }

    return func(..._memo)
  }

  return _internal()
}

module.exports = curry
