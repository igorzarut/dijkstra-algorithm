const {heapify} = require('./heapify')

class BinaryHeapAbstract {
  constructor (items = []) {
    this._items = Array.from(items)

    let index = Math.floor(this._items.length / 2) - 1

    for (; index >= 0; --index) {
      this._heapify(index)
    }
  }

  pop () {
    if (this._items.length === 0) {
      throw new Error('Cannot pop item from empty heap')
    }

    const rootItem = this._items[0]
    this._headToTail()
    return rootItem
  }

  insert (item) {
    const items = this._items

    items.push(item)

    if (items.length === 1) {
      return
    }

    const parent = index => Math.floor(index / 2)
    let insertElementIndex = items.length - 1

    // Всплывание вставленного элемента вверх по дереву.
    // TODO: Реализовать кастомизацию компаратора.
    while (
      insertElementIndex !== 0 &&
      items[parent(insertElementIndex)] > items[insertElementIndex]
    ) {
      const parentIndex = parent(insertElementIndex);

      [items[parentIndex], items[insertElementIndex]] =
      [items[insertElementIndex], items[parentIndex]]

      insertElementIndex = parent(insertElementIndex)
    }
  }

  getItems () {
    return this._items
  }

  get size () {
    return this.getItems().length
  }

  _heapify (index = 0) {
    const heapifyFunc = this._getHeapifyFunc()
    heapifyFunc(this._items, index)
  }

  _headToTail () {
    const items = this._items
    const head = items[0]
    const tail = items[items.length - 1]

    items[0] = tail
    items[items.length - 1] = head
  }

  _getHeapifyFunc () {
    return heapify(this._getCompareFunc())
  }

  _getCompareFunc () {
    throw new Error('Must be implemented in child class')
  }
}

class BinaryMinHeap extends BinaryHeapAbstract {
  _getCompareFunc () {
    return (a, b) => a < b
  }
}

class BinaryMaxHeap extends BinaryHeapAbstract {
  _getCompareFunc () {
    return (a, b) => a > b
  }
}

module.exports = BinaryHeapAbstract

module.exports.BinaryMinHeap = BinaryMinHeap
module.exports.BinaryMaxHeap = BinaryMaxHeap
