const curry = require('./curry')

const heapify = (compare, heap, index = 0) => {
  let floater = index

  let leftIndex = (2 * index) + 1
  let rightIndex = (2 * index) + 2

  const leftShouldFloat = () => compare(heap[leftIndex], heap[floater])

  if (hasLeafAtIndex(heap, leftIndex) && leftShouldFloat()) {
    floater = leftIndex
  }

  const rightShouldFloat = () => compare(heap[rightIndex], heap[floater])

  if (hasLeafAtIndex(heap, rightIndex) && rightShouldFloat()) {
    floater = rightIndex
  }

  if (floater !== index) {
    [heap[floater], heap[index]] = [heap[index], heap[floater]]

    heapify(compare, heap, floater)
  }
}

const hasLeafAtIndex = (heap, index) => index < heap.length

exports.heapify = curry(heapify)
