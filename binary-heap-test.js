// const {BinaryMinHeap} = require('./binary-heap')
const GraphNodeBinaryHeapMin = require('./graph-node-binary-heap-min')

const log = console.log

// const input = [1, 2, 3, 4, 5, 6, 7, 8, 9]

const makeNode = (value) => ({id: value, value, toString () { return this.value }})

const randomArray = Array.from(new Array(10), (item, index) => makeNode(Math.floor(Math.random() * 100)))

const binaryMinHeap = new GraphNodeBinaryHeapMin(randomArray)

const printHeap = (heap) => {
  const buffer = heap.reduce((buffer, item, index) => {
    const level = Math.floor(Math.log2(index + 1))
    const nextLevel = Math.floor(Math.log2(index + 2))

    buffer += item + ' '

    if (heap[index + 1] && nextLevel !== level) {
      buffer += '\n'
    }

    return buffer
  }, '')

  log(buffer)
}

printHeap(binaryMinHeap.getItems())
log('-'.repeat(20))
binaryMinHeap.insert(makeNode(3.5))
printHeap(binaryMinHeap.getItems())
