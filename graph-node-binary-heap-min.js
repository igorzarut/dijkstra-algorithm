const BinaryHeapAbstract = require('./binary-heap')

class GraphNodeBinaryHeapMin extends BinaryHeapAbstract {
  _getCompareFunc () {
    return (a, b) => a.value < b.value
  }
}

module.exports = GraphNodeBinaryHeapMin
