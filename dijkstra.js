const GraphNodeBinaryHeapMin = require('./graph-node-binary-heap-min')

const log = console.log

const graph = {
  vertices: [
    {id: 1},
    {id: 2},
    {id: 3},
    {id: 4},
    {id: 5},
    {id: 6}
  ],
  edges: [
    {vertices: [1, 2], weight: 7},
    {vertices: [1, 3], weight: 9},
    {vertices: [1, 6], weight: 14},
    {vertices: [2, 3], weight: 10},
    {vertices: [2, 4], weight: 15},
    {vertices: [3, 4], weight: 11},
    {vertices: [3, 6], weight: 2},
    {vertices: [4, 5], weight: 6},
    {vertices: [5, 6], weight: 9}
  ]
}

/* const findMin = (shortest, Q) => {
  log('#findMin, Q', Q)
  let minId = Q.entries().next().value[0]
  let minValue = shortest[minId]

  shortest.forEach((value, id) => {
    if (!Q.has(id)) return

    if (value < minValue) {
      minValue = value
      minId = id
    }
  })

  log('#findMin:', minId)
  log('#findMin (value):', minValue)

  return minId
} */

const findAdjacent = (edges, id) => {
  const adjacent = []

  edges.forEach(edge => {
    if (edge.vertices.includes(id)) {
      adjacent.push(edge)
    }
  })

  return adjacent
}

const getAnotherVertexId = (vertexIdArray, id) => {
  if (vertexIdArray[0] === id) {
    return vertexIdArray[1]
  } else if (vertexIdArray[1] === id) {
    return vertexIdArray[0]
  } else {
    throw new Error('vertexIdArray does not contain passed id')
  }
}

// const makeNode = (value) => ({id: value, value, toString () { return this.value }})

const dijkstraShortestPath = (graph, startVertexId) => {
  const shortest = (new Array(graph.vertices.length + 1)).fill(Infinity)
  const previous = (new Array(graph.vertices.length + 1)).fill(null)

  shortest[startVertexId] = 0

  // Строим двоичную кучу с вершинами и стоимостью перехода до них
  const unvisitedVertexesQueue = new GraphNodeBinaryHeapMin(
      graph.vertices.map((vertex) => ({
        ...vertex,
          // для запуска алгоритма задаем начальной вершине стоимость 0
        weight: vertex.id === startVertexId ? 0 : Infinity
      }))
  )

  // TODO: Привет из прошлого!
  // Вы сейчас наверняка вспоминаете на каком этапе остановились в прошлый раз?
  // Если кратко – осталось правильно реализовать метод .pop
  // –> сейчас он не удаляет первый элемент и не делает .heapify().
  // (сложность heapify – O(N * log N)).
  // !!! Идея: вспомните про добавление произвольного элемента в уже упорядоченную кучу.

  while (unvisitedVertexesQueue.size) {
    log('\n\n' + '-'.repeat(30) + '\n\n')
    log('Q:', unvisitedVertexesQueue)
    log('shortest:', shortest)
    log('previous:', previous)

    const vertexWithMinimumWeight = unvisitedVertexesQueue.pop()
    const minId = vertexWithMinimumWeight.id

    log('minId:', minId)
    log('vertexWithMinimumWeight:', vertexWithMinimumWeight)

    const adjacent = findAdjacent(graph.edges, vertexWithMinimumWeight.id)

    log('adjacent:', adjacent)

    adjacent.forEach(edge => {
      log('edge:', edge)

      // do relax
      const anotherVertexId = getAnotherVertexId(
        edge.vertices, vertexWithMinimumWeight.id
      )
      const shortestSum = shortest[vertexWithMinimumWeight.id] + edge.weight
      if (shortestSum < shortest[anotherVertexId]) {
        log('got shortest:', shortestSum)
        shortest[anotherVertexId] = shortestSum
        previous[anotherVertexId] = vertexWithMinimumWeight.id
      }
    })
  }

  return {
    shortest,
    pred: previous
  }
}

const {shortest, pred} = dijkstraShortestPath(graph, 1)
log('\n\nresult:\n\n', shortest)
pred.forEach((previousVertexId, nextVertexId) =>
  log(`${previousVertexId} => ${nextVertexId}`)
)

const correctResult = `
correct result:

[ Infinity, 0, 7, 9, 20, 20, 11 ]
null => 0
null => 1
1 => 2
1 => 3
3 => 4
6 => 5
3 => 6
`

log(correctResult)
